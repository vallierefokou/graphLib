/**
 * 
 */
package algorithms;

import java.util.HashMap;
import java.util.Timer;
import java.util.Map.Entry;

import util.*;


/**
 * Constants est la classe contenant les variables utilis�es dans le projet
 * 
 * @author riesen - valliere
 */
public class Constants {

	/**
	 * the eps-components
	 */
	public final static GraphComponent EPS_COMPONENT = new GraphComponent(
			Constants.EPS_ID);

	public final static String EPS_ID = "eps_id";
	
	public static Graph g1,g2;
	
	public static EditPath ROOT;
	
	public static EditPath UB,UB_min, UB_minsecond; // the best matching (node to node and vertex to vertex) found so far

	public static double UBCOST, UB_minCOST, UB_minsecondCOST; // the best distance found so far
	
	public static EditPath UBSelected;
	/**
	 * the cost function has to be changed for different databases (see 1. todo)
	 */
	public static ICostFunction costFunction;

	/**
	 * the edgehandler has to be changed for several databases (see 2. todo)
	 */
	public static IEdgeHandler edgeHandler;

	public static double[][] nodecostmatrix=null;

	public static double[][] edgecostmatrix=null;
	
	public static long timeconstraint=60000;
	public static long memoryconstraint=100000;
	public static  Timer timer;	
	public static int MegaBytes = 1048576;

	public static EditPath FirstUB=null;
	

	public Constants(double nodeCosts, double edgeCosts, double alpha) {
		// set the appropriate cost function
		Constants.costFunction = (ICostFunction)new UnlabeledCostFunction(nodeCosts, edgeCosts, alpha); // TODO 1
                // set the appropriate edgehandler (directed or undirected)
                Constants.edgeHandler = new UniversalEdgeHandler(); 
	}
	
	

}
