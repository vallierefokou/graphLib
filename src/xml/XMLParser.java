package xml;



import java.io.FileReader;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;

import nanoxml.XMLElement;

import util.Edge;
import util.Graph;
import util.GraphCollection;
import util.Node;

//classe qui permet de parser le fichier xml contenant les donn�es du graphe
public class XMLParser {

	private String graphPath;

	public String getGraphPath() {
		return graphPath;
	}

	public void setGraphPath(String graphPath) {
		this.graphPath = graphPath;
	}
	
	//permet de creer une liste de graphes � partir des fichiers pars�s
	public GraphCollection parseCXL(String filename) throws Exception {

		XMLElement xml = new XMLElement();
		FileReader reader = new FileReader(filename);
		xml.parseFromReader(reader);

		GraphCollection graphCollection = new GraphCollection();
		graphCollection.setCollectionName(filename);
		Vector children = xml.getChildren();
		XMLElement root = (XMLElement) children.get(0);
		Enumeration enumerator = root.enumerateChildren();
		while (enumerator.hasMoreElements()) {
			XMLElement child = (XMLElement) enumerator.nextElement();
			Graph g = this.parseGXL(this.graphPath
					+ child.getAttribute("file", null)+"");
			g.setClassId((String) child.getAttribute("class", "NO_CLASS"));
			graphCollection.add(g);
		}
		return graphCollection;
	}

	// Cette fonction prend en param�tre un fichier de donn�es de graphe, et retourne le graphe correspondant
	public Graph parseGXL(String filename) throws Exception {

		XMLElement xml = new XMLElement();
		FileReader reader = new FileReader(filename);
		xml.parseFromReader(reader);
		reader.close();
		Graph graph1 = new Graph();
		Vector children = xml.getChildren(); //contient le contenu du fichier xml � partir de la balise graph
		XMLElement root = (XMLElement) children.get(0); //root a le m�me contenu que children
		String id = (String) root.getAttribute("id", null); //id contient la valeur contenue dans la ppt� id de la balise graph du fichier
		String edgeids = (String) root.getAttribute("edgeids", null);  // contient la valeur contenue dans la ppt� edgeids de la balise graph (null par d�faut si �a n'existe pas) 
		String edgemode = (String) root.getAttribute("edgemode", "undirected"); // contient la valeur contenue dans la ppt� edgemode de la balise graph (undirected par d�faut si �a n'existe pas)
		//on affecte ces ppt�s lues � notre objet repr�sentant le graph1
		graph1.setId(id);
		graph1.setEdgeId(edgeids);
		graph1.setEdgeMode(edgemode);

		Enumeration enumerator = root.enumerateChildren(); //compter toutes les balises situ�es apr�s la balise graph
		while (enumerator.hasMoreElements()) { 
			XMLElement child = (XMLElement) enumerator.nextElement(); //r�cuperer chacune de ces balises
			if (child.getName().equals("node")) { //si cette balise s'appelle node
				String nodeId = (String) (child.getAttribute("id", null)); //on recup�re la ppt� id du node
				
				//on cree un noeud en indiquant les ppt�s lues
				Node node = new Node(edgemode);  
				node.setId(nodeId);

				Enumeration enum1 = child.enumerateChildren(); //compte les balises situ�es dans la balise node
				while (enum1.hasMoreElements()) {
					XMLElement child1 = (XMLElement) enum1.nextElement(); //on r�cup�re chaque �lement
					if (child1.getName().equals("attr")) { //si cette balise s'appelle attr
						String key = (String) child1.getAttribute("name", null); //on r�cup�re la valeur de sa ppt� name (null par d�faut si �a n'existe pas)
						Vector children2 = child1.getChildren(); //on r�cup�re la balise situ�e dans la balise attr
						XMLElement child2 = (XMLElement) children2.get(0); 

						String value = child2.getContent();  //on r�cup�re la valeur de l'�lement
						node.put(key, value);  //on enregistre dans le noeud, l'attribut lu
					}

				}
				graph1.add(node);
			}
			if (child.getName().equals("edge")) { //si cette balise s'appelle edge
				Edge edge = new Edge(edgemode);  //on cree un edge en fonction du edgemode lu au d�but du graphe
				String from = (String) child.getAttribute("from", null); //lecture de la valeur de la ppt� from
				String to = (String) child.getAttribute("to", null);  //lecture de la valeur de la to
				//on enregistre ces ppt�s dans l'objet edge
				edge.put("from", from); 
				edge.put("to", to);
				edge.setId(from + "_<>" + to); //l'id de l'edge est constitu� de la valeur du from et du to
				// *******************************
				Enumeration enum1 = child.enumerateChildren();  //compte les balises situ�es dans la balise edge
				while (enum1.hasMoreElements()) {
					XMLElement child1 = (XMLElement) enum1.nextElement(); //on recup�re chaque �lement
					if (child1.getName().equals("attr")) {  //si cette balise s'appelle attr
						String key = (String) child1.getAttribute("name",
								"key failed!");  //on r�cup�re la valeur de sa ppt� name (key failed par d�faut si �a n'existe pas)
						Vector children2 = child1.getChildren();  //on r�cup�re la balise situ�e dans la balise attr
						XMLElement child2 = (XMLElement) children2.get(0);
						String value = child2.getContent();  //on r�cup�re la valeur de l'�lement
						edge.put(key, value);  //on enregistre dans l'edge, l'attribut lu
					}

				}
				Iterator nodeIterator = graph1.iterator();  //compter le nombre de noeuds du graphe
				while (nodeIterator.hasNext()) {  //pour chaque edge courant, relier avec les nodes correspondants
					Node node = (Node) nodeIterator.next();
					if (node.getComponentId().equals(from)) {
						edge.setStartNode(node);
						node.getEdges().add(edge);
					}
					if (node.getComponentId().equals(to)) {
						edge.setEndNode(node);
						node.getEdges().add(edge);
					}
				}
				graph1.getEdges().add(edge);  //on ajoute l'edge courant au graphe
			}
		}
		
	
		return graph1;
	}
	
	

	private void printGraph(Graph g) {
		System.out.println("The Graph: "+g.getId()+" (Label="+g.getClassId()+")\n");
		System.out.println("Nodes:");
		for (int i = 0; i < g.size(); i++){
			Node n = (Node) g.get(i);
			System.out.println(n.getComponentId());
			System.out.println(n.getTable().get("FREQUENCY"));
		}
		System.exit(0);
	}

	
}
