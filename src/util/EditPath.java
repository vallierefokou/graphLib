package util;

import java.io.IOException;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.util.*;
import java.util.Map.Entry;

import algorithms.Constants;
import algorithms.MatrixGenerator;
import algorithms.Munkres;
import algorithms.MunkresRec;


/**
 * @author Romain Raveaux
 * 
 */
public class EditPath  {



	/**
	 * the total costs of this path
	 */
	 double totalCosts;

	static public int NoHeuristic=0;
	static public int NoAssigmentHeuristic=1;
	static public int MunkresAssigmentHeuristic=2;
	static public int LAPAssigmentHeuristic=3;
	static public int HausdorffEDHeuristic = 4;
	//public static int BestFirstAssigmentHeuristic = 4;

	
	private MunkresRec munkresRec;
	private int noOfDistortedNodes;

	private String editPathId;

	private double HeuristicCosts;
	private boolean isheuristiccomputed;


	public double getHeuristicCosts() {
		return HeuristicCosts;
	}

	public int getNoOfDistortedNodes()
	{
		return noOfDistortedNodes;
	}

	public void setID(String editPathId)
	{
		this.editPathId = editPathId;
	}

	public String getID()
	{
		return editPathId;
	}

	public void setNoOfDistortedNodes(int noOfDistortedNodes)
	{
		this.noOfDistortedNodes = noOfDistortedNodes;
	}

	public void setHeuristicCosts(double heuristicCosts) {
		HeuristicCosts = heuristicCosts;
	}


	public double ComputeHeuristicCosts(int heuristicmethod) {

		if(this.isheuristiccomputed==false){
			if(heuristicmethod==MunkresAssigmentHeuristic) ComputeHeuristicCostsAssignmentMunkres();
			if(heuristicmethod == NoAssigmentHeuristic ) ComputeNoAssigmentHeuristic();
			if(heuristicmethod==NoHeuristic) HeuristicCosts=0;
			isheuristiccomputed=true;
		}
		return HeuristicCosts;

	}



	
	public double ComputeNoAssigmentHeuristic() {

		HeuristicCosts=0.0;
		int n1,n2;
		int e1,e2;
		n1 = this.unUsedNodes1.size();
		n2 = this.unUsedNodes2.size();

		e1 = this.unUsedEdges1.size();
		e2 = this.unUsedEdges2.size();
		//System.out.println("********************************************");

		int nbdeletions=Math.max(0, n1-n2);
		HeuristicCosts += ComputeNbDeletion(unUsedNodes1,nbdeletions);
		int nbinsertion=Math.max(0,n2-n1);
		HeuristicCosts += ComputeNbInsertion(unUsedNodes2,nbinsertion);

		nbdeletions=Math.max(0, e1-e2);
		HeuristicCosts += ComputeNbDeletion(unUsedEdges1,nbdeletions);
		nbinsertion=Math.max(0,e2-e1);
		HeuristicCosts += ComputeNbInsertion(unUsedEdges2,nbinsertion);


		return HeuristicCosts;
	}


	/**
	 * constructs a new editpath between
	 * 
	 * @param source
	 *            and
	 * @param target
	 */
	public EditPath(Graph source, Graph target, int heuristicmethod, boolean sortG1) {
	

		this.heuristicmethod = heuristicmethod;
		//System.out.println("heuristicmethod ++++++"+heuristicmethod);
		munkresRec = new MunkresRec();
		this.init();
		this.source = source;
		this.target = target;
		LinkedList tmpunUsedNodes1 = new LinkedList();
		this.unUsedNodes1 = new LinkedList();
		this.unUsedNodes2.addAll(target);
		this.unUsedEdges1.addAll(source.getEdges());
		this.unUsedEdges2.addAll(target.getEdges());
		
		
		if(sortG1==false)
		{
			this.unUsedNodes1.addAll(source);

			//calcul des couts entre tous les noeuds et les liens
			//pour que les couts ne soit calculer qu'une fois.
		     if( Constants.nodecostmatrix==null || Constants.edgecostmatrix==null){
				Constants.nodecostmatrix = ComputeCostMatrixNM(unUsedNodes1,unUsedNodes2,true);  //on initialise la matrice de co�t des noeuds
				Constants.edgecostmatrix = ComputeCostMatrixNM(unUsedEdges1,unUsedEdges2,false); //on initialise la matrice de co�t des edges
		     }
			
			
		}
		else
		{
			tmpunUsedNodes1.addAll(source);
			//calcul des couts entre tous les noeuds
			//pour que les couts ne soit calculer qu'une fois.
			if( Constants.nodecostmatrix==null || Constants.edgecostmatrix==null){
				Constants.nodecostmatrix = ComputeCostMatrixNM(tmpunUsedNodes1,unUsedNodes2,true);
				Constants.edgecostmatrix = ComputeCostMatrixNM(unUsedEdges1,unUsedEdges2,false);
			}

			sortunUsedNodes1(tmpunUsedNodes1);
		}


	}
	
	



	private void sortunUsedNodes1(LinkedList tmpunUsedNodes1) {
		// TODO Auto-generated method stub
		double[][] matrix = this.ComputeCostMatrix(tmpunUsedNodes1, unUsedNodes2, true);
		int[][] starredmatrix = this.ComputeStarredMatrix(matrix, true);

		for(int i=0;i<matrix.length;i++){

			double minval=Double.POSITIVE_INFINITY;
			int minindex=-1;

			for(int j=0;j<starredmatrix.length;j++){

				int r = starredmatrix[j][0];
				int c = starredmatrix[j][1];

				if(minval>=matrix[r][c]){
					minval = matrix[r][c];
					minindex = j;

				}

			}

			int r = starredmatrix[minindex][0];
			int c = starredmatrix[minindex][1];
			matrix[r][c]=Double.POSITIVE_INFINITY;
			GraphComponent gc1;
			if(r>= tmpunUsedNodes1.size() ) {
				gc1=Constants.EPS_COMPONENT;
			}else{
				gc1 = (GraphComponent) tmpunUsedNodes1.get(r);
				this.unUsedNodes1.addLast(gc1);
			}
		}	




	}

	private HashMap<GraphComponent, GraphComponent> ComputeBestMatchGC(
			int[][] starredNode2, LinkedList unUsedNodes22, boolean b) {
		// TODO Auto-generated method stub
		int k = starredNode2.length;
		HashMap<GraphComponent,GraphComponent> BestMatchNodeG2a = new HashMap<GraphComponent,GraphComponent>();
		for(int i=0;i<unUsedNodes22.size();i++){
			GraphComponent gc = (GraphComponent) unUsedNodes22.get(i);
			for(int j=0;j<k;j++){

				int r = starredNode2[j][0];
				int c = starredNode2[j][1];
				if(c == i){
					GraphComponent gc1;

					if(b == true ){
						if(r>= this.source.size() ) {
							gc1=Constants.EPS_COMPONENT;
						}else{
							gc1 = (GraphComponent) this.source.get(r);
						}
					}else{
						if(r>= this.source.getEdges().size() ) {
							gc1=Constants.EPS_COMPONENT;
						}else{
							gc1 = (GraphComponent) this.source.getEdges().get(r);
						}
					}
					BestMatchNodeG2a.put(gc, gc1);
					j=k;
				}

			}

		}
		return BestMatchNodeG2a;
	}


	private int[][] ComputeStarredMatrix(double[][] nodecostmatric,boolean isnode) {
		// TODO Auto-generated method stub
			
		MatrixGenerator mgen = new MatrixGenerator();
		Munkres munkres = new Munkres();
		MunkresRec munkresRec = new MunkresRec();
		mgen.setMunkres(munkresRec);
		
		double[][] matrix = mgen.getMatrix(source, target);
		munkres.setGraphs(source, target);
		double cost = munkres.getCosts(matrix);
		Constants.FirstUB=munkres.getBestEditPath();
		
		return munkres.getStarredIndices();
	}


	private double[][] ComputeCostMatrixNM(LinkedList unUsedNodes12,
			LinkedList unUsedNodes22,boolean isnode) {
		// TODO Auto-generated method stub


		int n1 = unUsedNodes12.size(); //nombre de noeuds/edges du graphe 1
		int n2 = unUsedNodes22.size(); //nombre de noeuds/edges du graphe 2

		GraphComponent epsn = Constants.EPS_COMPONENT;//new GraphComponent(Constants.EPS_ID) ;
		//	epsn.setNode(isnode);

		GraphComponent node1;
		GraphComponent node2;

		double[][] matrix = new double[n1+2][n2+2];
		//on compare chaque noeud/edge du graphe1 avec tous les noeuds du graphe 2
		for (int i = 0; i < n1; i++){   
			node1 = (GraphComponent)unUsedNodes12.get(i);	//on r�cup�re chaque noeud/edge du graphe1 avec ses propri�t�s
			node1.id=i;
			node1.belongtosourcegraph=true;
			//node1.setBelongtosourcegraph(1);
			for (int j = 0; j < n2; j++){
				node2 = (GraphComponent)unUsedNodes22.get(j);   //on r�cup�re chaque noeud/edge du graphe2 avec ses propri�t�s
				node2.id=j;
				node2.belongtosourcegraph=false;
				//node2.setBelongtosourcegraph(0);
				matrix[i][j]=(float)Constants.costFunction.getCosts(node1, node2);  //on initialise chaque �lement de la matrice de co�t des noeuds/edge
			}

		}

		for (int j = 0; j < n2; j++){    
			node2 = (GraphComponent)unUsedNodes22.get(j); //on r�cup�re chaque noeud/edge du graphe2 avec ses propri�t�s
			matrix[n1][j]=(float)Constants.costFunction.getCosts(Constants.EPS_COMPONENT, node2); //toutes les valeurs de cette ligne sont �gales � (45 noeud inexistant pour start ou 7.5 edge inexistant pour start)
		}

		for (int j = 0; j < n2; j++){
			node2 = (GraphComponent)unUsedNodes22.get(j); //on r�cup�re chaque noeud/edge du graphe2 avec ses propri�t�s
			matrix[n1+1][j]=(float)Constants.costFunction.getCosts(node2, Constants.EPS_COMPONENT); //toutes les valeurs de cette ligne sont �gales � (45 noeud inexistant pour end ou 7.5 edge inexistant pour end)
		}

		for (int i = 0; i < n1; i++){
			node1 = (GraphComponent)unUsedNodes12.get(i);	//on r�cup�re chaque noeud/edge du graphe1 avec ses propri�t�s
			matrix[i][n2]=(float)Constants.costFunction.getCosts(Constants.EPS_COMPONENT, node1); //toutes les valeurs de cette ligne sont �gales � (45 noeud inexistant pour start ou 7.5 edge inexistant pour start)
		}

		for (int i = 0; i < n1; i++){
			node1 = (GraphComponent)unUsedNodes12.get(i);
			matrix[i][n2+1]=(float)Constants.costFunction.getCosts(node1, Constants.EPS_COMPONENT); //toutes les valeurs de cette ligne sont �gales � (45 noeud inexistant pour end ou 7.5 edge inexistant pour end)
		}




		return matrix;
	}

	private double[][] ComputeCostMatrix(LinkedList unUsedNodes12,
			LinkedList unUsedNodes22,boolean isnode) {
		// TODO Auto-generated method stub


		int sSize = unUsedNodes12.size();
		int tSize = unUsedNodes22.size();
		int dim = sSize + tSize;

		double[][] matrix = new double[dim][dim];
		double[][] edgeMatrix;
		Node u;
		Node v;
		for (int i = 0; i < sSize; i++) {
			u = (Node) this.source.get(i);
			for (int j = 0; j < tSize; j++) {
				v = (Node) this.target.get(j);
				double costs = Constants.costFunction.getCosts(u, v);
				// adjacency information
				edgeMatrix = this.getEdgeMatrix(u, v);
				costs += this.munkresRec.getCosts(edgeMatrix);
				matrix[i][j] = costs;
			}
		}
		for (int i = sSize; i < dim; i++) {
			for (int j = 0; j < tSize; j++) {
				if ((i - sSize) == j) {
					v = (Node) this.target.get(j);
					double costs = Constants.costFunction.getCosts(
							Constants.EPS_COMPONENT, v);
					for(int k=0;k<v.getEdges().size();k++){
						Edge e = (Edge) v.getEdges().get(k);
						costs += Constants.costFunction.getCosts(
								Constants.EPS_COMPONENT, e);
					}
					//double f = v.getEdges().size();
					//costs += (f * Constants.costFunction.getEdgeCosts());
					
					
					matrix[i][j] = costs;
				} else {
					matrix[i][j] = Double.POSITIVE_INFINITY;
				}
			}
		}
		for (int i = 0; i < sSize; i++) {
			u = (Node) this.source.get(i);
			for (int j = tSize; j < dim; j++) {
				if ((j - tSize) == i) {
					double costs = Constants.costFunction.getCosts(u,
							Constants.EPS_COMPONENT);
					//double f = u.getEdges().size();
					//costs += (f * Constants.costFunction.getEdgeCosts());
					for(int k=0;k<u.getEdges().size();k++){
						Edge e = (Edge) u.getEdges().get(k);
						costs += Constants.costFunction.getCosts(e,
								Constants.EPS_COMPONENT);
					}
					matrix[i][j] = costs;
				} else {
					matrix[i][j] = Double.POSITIVE_INFINITY;
				}
			}
		}
		for (int i = sSize; i < dim; i++) {
			for (int j = tSize; j < dim; j++) {
				matrix[i][j] =Double.POSITIVE_INFINITY;
			}
		}
		return matrix;
	}
	
	
	private double[][] getEdgeMatrix(Node u, Node v) {
		int uSize = u.getEdges().size();
		int vSize = v.getEdges().size();
		int dim = uSize + vSize;
		double[][] edgeMatrix = new double[dim][dim];
		Edge e_u;
		Edge e_v;
		for (int i = 0; i < uSize; i++) {
			e_u = (Edge) u.getEdges().get(i);
			for (int j = 0; j < vSize; j++) {
				e_v = (Edge) v.getEdges().get(j);
				double costs = Constants.costFunction.getCosts(e_u, e_v);
				edgeMatrix[i][j] = costs;
			}
		}
		for (int i = uSize; i < dim; i++) {
			for (int j = 0; j < vSize; j++) {
				// diagonal
				if ((i - uSize) == j) {
					e_v = (Edge) v.getEdges().get(j);
					double costs = Constants.costFunction.getCosts(
							Constants.EPS_COMPONENT, e_v);
					edgeMatrix[i][j] = costs;
				} else {
					edgeMatrix[i][j] = Double.POSITIVE_INFINITY;
				}
			}
		}
		for (int i = 0; i < uSize; i++) {
			e_u = (Edge) u.getEdges().get(i);
			for (int j = vSize; j < dim; j++) {
				// diagonal
				if ((j - vSize) == i) {
					double costs = Constants.costFunction.getCosts(e_u,
							Constants.EPS_COMPONENT);
					edgeMatrix[i][j] = costs;
				} else {
					edgeMatrix[i][j] = Double.POSITIVE_INFINITY;
				}
			}
		}
		for (int i = uSize; i < dim; i++) {
			for (int j = vSize; j < dim; j++) {
				edgeMatrix[i][j] = 0.0;
			}
		}
		return edgeMatrix;
	}

	/**
	 * constructs a new editpath from a string str
	 *  @param String Format: (HeuristicCosts<>totalCosts<>getId1<>getId2<>getEdgesId1<>
	 *  getEdgesId2<>getEdgesMode1<>getEdgesMode2<>unUsedNodes1<>
	 *  unUsedNodes2<>unUsedEdges1<>unUsedEdges2<>Distortions<>invertedEdges)

	 */

	private double ComputeNbDeletion(LinkedList unUsedNodes12, int nbdeletions) {
		// TODO Auto-generated method stub
		GraphComponent node2 = Constants.EPS_COMPONENT;//new GraphComponent(Constants.EPS_ID) ;
		double res=0.0;

		for(int i=0;i<nbdeletions;i++){
			GraphComponent node1 = (GraphComponent) unUsedNodes12.get(i);
			res+=Constants.costFunction.getCosts(node1, node2);
		}
		return res;
	}

	private double ComputeNbInsertion(LinkedList unUsedNodes12, int nbdeletions) {
		// TODO Auto-generated method stub
		GraphComponent node2 = Constants.EPS_COMPONENT;//new GraphComponent(Constants.EPS_ID) ;
		double res=0.0;

		for(int i=0;i<nbdeletions;i++){
			GraphComponent node1 = (GraphComponent) unUsedNodes12.get(i);
			res+=Constants.costFunction.getCosts(node2,node1 );
		}
		return res;
	}



	private double ComputeMinSubstituionMunkres(LinkedList unUsedNodes12,
			LinkedList unUsedNodes22) {
		// TODO Auto-generated method stub
		//LAPJonkerVolgenantAssignment LAP; 
		//LAP = new LAPJonkerVolgenantAssignment();
		int n = Math.max(unUsedNodes12.size(), unUsedNodes22.size());
		double[][] matrix = new double[n][n];
		int[] rowsol,colsol;
		double[] u,v;
		u=new double[n];
		v=new double[n];
		rowsol = new int[n];
		colsol = new int[n];

		for (int i = 0; i < n; i++){
			GraphComponent node1 = new GraphComponent(Constants.EPS_ID) ;
			if(i<unUsedNodes12.size()){
				node1 = (GraphComponent)unUsedNodes12.get(i);

			}else{
				node1 = Constants.EPS_COMPONENT;
			}

			for (int j = 0; j < n; j++){
				GraphComponent node2 = new GraphComponent(Constants.EPS_ID) ;
				if(j<unUsedNodes22.size()){
					node2 = (GraphComponent)unUsedNodes22.get(j);

				}else{
					node2 = Constants.EPS_COMPONENT;
				}

				double minval=Constants.costFunction.getCosts(node1, node2);
				GraphComponent epsgc = Constants.EPS_COMPONENT;
			
				if(!node1.equals(Constants.EPS_COMPONENT) && !node2.equals(Constants.EPS_COMPONENT)){
					double valdel = Constants.costFunction.getCosts(node1,epsgc);
					//minval = Math.min(minval, valdel);
					double valins = Constants.costFunction.getCosts(epsgc,node2);
					minval = Math.min(minval, valdel+valins);
				}

				matrix[i][j]=minval;
			
			}

		}
	
		MunkresRec munkresRec;
		munkresRec = new MunkresRec();
		double cost=munkresRec.getCosts(matrix);
		return cost;

	}

	/**
	 * the unused nodes of both graphs
	 */
	private LinkedList unUsedNodes1, unUsedNodes2;

	/**
	 * the unused edges of both grpahs
	 */
	private LinkedList unUsedEdges1, unUsedEdges2;

	/**
	 * saves all distortions key = source graph-component value = target
	 * graph-component
	 */
	public Hashtable distortions;

	private Hashtable EdgeInverted;
	public Hashtable getEdgeInverted() {
		return EdgeInverted;
	}

	public void setEdgeInverted(Hashtable edgeInverted) {
		EdgeInverted = edgeInverted;
	}

	public static Graph source, target;

	private int heuristicmethod;



	public int getHeuristicmethod() {
		return heuristicmethod;
	}

	public void setHeuristicmethod(int heuristicmethod) {
		this.heuristicmethod = heuristicmethod;
	}

	/**
	 * constructs a new editpath between
	 * 
	 * @param source
	 *            and
	 * @param target
	 */
	public EditPath(Graph source, Graph target) {

		this.init();
		this.source = source;
		this.target = target;

		this.unUsedNodes1.addAll(source);
		this.unUsedNodes2.addAll(target);
		this.unUsedEdges1.addAll(source.getEdges());
		this.unUsedEdges2.addAll(target.getEdges());
		
		this.setHeuristicmethod(NoHeuristic);


	}

	


	
	

	/**
	 * copy constructor for an existing edit path
	 * 
	 * @param e
	 */
	public EditPath(EditPath e) {
		this.init();
		this.source = e.getSource();
		this.target = e.getTarget();
		this.unUsedNodes1.addAll(e.getUnUsedNodes1());
		this.unUsedNodes2.addAll(e.getUnUsedNodes2());
		this.unUsedEdges1.addAll(e.getUnUsedEdges1());
		this.unUsedEdges2.addAll(e.getUnUsedEdges2());
		this.totalCosts = e.getTotalCosts();
		this.distortions.putAll(e.getDistortions());
		this.EdgeInverted.putAll(e.getEdgeInverted());
		this.noOfDistortedNodes=e.getNoOfDistortedNodes();
		this.HeuristicCosts = e.HeuristicCosts;
		this.isheuristiccomputed=e.isheuristiccomputed;
		this.heuristicmethod=e.heuristicmethod;
		//System.out.println("heuristic method "+heuristicmethod);


	}

	public EditPath() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * helper method for both constructors
	 */
	private void init() {
		this.unUsedEdges1 = new LinkedList();
		this.unUsedEdges2 = new LinkedList();
		this.unUsedNodes1 = new LinkedList();
		this.unUsedNodes2 = new LinkedList();
		this.distortions = new Hashtable();
		EdgeInverted = new Hashtable();
		this.totalCosts = 0.0;
		this.noOfDistortedNodes=0;
		HeuristicCosts=0.0;
		this.isheuristiccomputed=false;

	}

	/**
	 * adds a new distortion to this path
	 * 
	 * @param sComp
	 * @param tComp
	 */
	public void addDistortion(GraphComponent sComp, GraphComponent tComp) {
		
		
		isheuristiccomputed=false;
		this.totalCosts += Constants.costFunction.getCosts(sComp, tComp);
		if (sComp.getComponentId().equals(Constants.EPS_ID)) {
			GraphComponent eps = new GraphComponent(Constants.EPS_ID);
			//System.out.println("****DISTORTION : ("+eps.getComponentId()+" , "+tComp.getComponentId()+")");
			this.distortions.put(eps, tComp);
		}else if(tComp.getComponentId().equals(Constants.EPS_ID)){
			GraphComponent eps = new GraphComponent(Constants.EPS_ID);
			this.distortions.put(sComp,eps);
		} 
		else {
			//System.out.println("****DISTORTION : ("+sComp.getComponentId()+" , "+tComp.getComponentId()+")");
			this.distortions.put(sComp, tComp);
		}

		if(sComp.isNode()==true){
			this.noOfDistortedNodes++;	
		}

		if (sComp.isNode() || tComp.isNode()) {

			this.unUsedNodes1.remove(sComp);
			// edgehandling (only once and only if comp=node!)
			//System.out.println("handling edges between "+sComp);
			Constants.edgeHandler.handleEdges(this, sComp, tComp);
		} else {
			this.unUsedEdges1.remove(sComp);
		}
		if (tComp.isNode()) {
			this.unUsedNodes2.remove(tComp);
		} else {
			this.unUsedEdges2.remove(tComp);
		}
		
	}

	/**
	 * @return true if this path is complete
	 */
	public boolean isComplete() {
		int remaining = this.unUsedNodes1.size();
		remaining += this.unUsedNodes2.size();
		return (remaining == 0);
	}

	/**
	 * @return true if this path has not used all start nodes yet
	 */
	public boolean hasStartNodes() {
		return (this.unUsedNodes1.size() > 0);
	}

	/**
	 * @return the total costs of this path
	 */
	public double getTotalCosts() {

		return totalCosts;
	}

	/**
	 * getters for the unused graphcomponents of this path
	 */
	public LinkedList getUnUsedNodes1() {
		return unUsedNodes1;
	}

	public LinkedList getUnUsedNodes2() {
		return unUsedNodes2;
	}

	public LinkedList getUnUsedEdges1() {
		return unUsedEdges1;
	}

	public LinkedList getUnUsedEdges2() {
		return unUsedEdges2;
	}

	/**
	 * @return the next node of source graph
	 */
	public Node getNext() {
		return (Node) this.unUsedNodes1.getFirst();
	}

	public Node getNextG2() {
		return (Node) this.unUsedNodes2.getFirst();
	}

	/**
	 * completes the actual path by inserting the remaining nodes of target
	 */
	public void complete() {
		LinkedList tempList = new LinkedList();
		tempList.addAll(this.unUsedNodes2);
		Iterator iter = tempList.iterator();
		while (iter.hasNext()) {
			Node w = (Node) iter.next();
			this.addDistortion(Constants.EPS_COMPONENT, w);
		}



	}

	/**
	 * @return the mappings
	 */
	public Hashtable getDistortions() {
		return distortions;
	}

	/**
	 * @return the origin of
	 * @param mapped
	 */
	public GraphComponent getStart(GraphComponent mapped) {
		Enumeration enumeration = this.distortions.keys();
		while (enumeration.hasMoreElements()) {
			GraphComponent key = (GraphComponent) enumeration.nextElement();
			GraphComponent value = (GraphComponent) this.distortions.get(key);
			if (value.equals(mapped)) {
				return key;
			}
		}
		return null;
	}


	/**
	 * helper method for debuging: prints the path in the std. output
	 */
	public void printMe() {
		System.out.println("source-label: "+this.source.getId());
		System.out.println("target-label: "+this.target.getId());
	
		Enumeration enumeration = this.distortions.keys();
		while (enumeration.hasMoreElements()) {
			GraphComponent key = (GraphComponent) enumeration.nextElement();
			GraphComponent value = (GraphComponent) this.distortions.get(key);
			System.out.print(key.getComponentId() + "\t --> \t"+ value.getComponentId()+"\t\t"+Constants.costFunction.getCosts(key, value));
			System.out.println();

		}

	}

	/**
	 * asserts that all components are used
	 * 
	 * @return true if all components are used
	 */
	public boolean allUsed() {
		int remaining = this.unUsedNodes1.size();
		remaining += this.unUsedNodes2.size();
		remaining += this.unUsedEdges1.size();
		remaining += this.unUsedEdges2.size();
		if (remaining != 0){
			System.out.println(((Edge) this.unUsedEdges2.getFirst()).getComponentId());
		}
		return (remaining == 0);
	}

	public int getNumberOfNodeOps(){
		int space = 0;
		Enumeration enumeration = this.distortions.keys();
		while (enumeration.hasMoreElements()) {
			GraphComponent key = (GraphComponent) enumeration.nextElement();
			GraphComponent value = (GraphComponent) this.distortions.get(key);
			if (key.isNode() || value.isNode()){
				space++;
			}
		}
		return space;
	}

	public Graph getSource() {
		return source;
	}

	public Graph getTarget() {
		return target;
	}

	public void setSource(Graph source) {
		this.source = source;
	}

	public void setTarget(Graph target) {
		this.target = target;
	}

	public double ComputeHeuristicCostsAssignmentMunkres() {
		// TODO Auto-generated method stub
		HeuristicCosts=0.0;
		int n1,n2;
		int e1,e2;
		n1 = this.unUsedNodes1.size();
		n2 = this.unUsedNodes2.size();

		e1 = this.unUsedEdges1.size();
		e2 = this.unUsedEdges2.size();
		//System.out.println("********************************************");

		double leastexpensivenodesub = ComputeMinSubstituionMunkres(unUsedNodes1,unUsedNodes2);
		HeuristicCosts = leastexpensivenodesub;

		double leastexpensiveedgesub = ComputeMinSubstituionMunkres(unUsedEdges1,unUsedEdges2);
		HeuristicCosts += leastexpensiveedgesub;

	

		return HeuristicCosts;

	}

	public String bestMatchingNodesMapping()
	{
		String mappings="";
		
		Enumeration enumeration = this.distortions.keys();
		while (enumeration.hasMoreElements()) {
			GraphComponent key = (GraphComponent) enumeration.nextElement();
			GraphComponent value = (GraphComponent) this.distortions.get(key);
			
		
			if(key.isNode()==true || value.isNode()==true)
			{
				mappings = mappings.concat("Node:");
			}
			else	
			{
				mappings = mappings.concat("Edge:");
			}
			
			mappings = mappings.concat(key.getComponentId() + "->"+ value.getComponentId()+"="+Constants.costFunction.getCosts(key, value)+"/");





		}
		
		return mappings;
	}

	
	

	
	
	public String g2Indices1(Graph G1)
	{
		String mappings="";
		
		
		for(int i=0; i<G1.size(); i++)
		{
			
			Node node = (Node) G1.get(i);

			String node1 = node.getComponentId().toString();
			
			
			Enumeration enumeration = this.distortions.keys();
			while (enumeration.hasMoreElements()) {
				GraphComponent key = (GraphComponent) enumeration.nextElement();
				
				if (key.isNode()==true)
				{
					if(key.getComponentId().equals(node1))
					{
						
						GraphComponent value = (GraphComponent) this.distortions.get(key);

			
						// Deletion of key (key -> epsilon)
						if(value.getComponentId().equals(Constants.EPS_ID))
						{
							mappings = mappings.concat("-1 ");
						}
						else if (!value.getComponentId().equals(Constants.EPS_ID))
						{
							mappings = mappings.concat(value.getComponentId()+" ");
						}
						else
						{
							
						}
						
				        break;
					}
				}
			
				
				

		

			}
			
		}
	
		return mappings;
	}
	
	
	
	public void setheuristiccomputed(boolean b) {
		// TODO Auto-generated method stub
		this.isheuristiccomputed=b;
	}

	public double ComputeTotalCostsWithoutDeletionAndInsertion() {
		// TODO Auto-generated method stub
		
		double res =Double.MAX_VALUE; 
		double count=0;
		Enumeration enumeration = this.distortions.keys();
		while (enumeration.hasMoreElements()) {
			GraphComponent key = (GraphComponent) enumeration.nextElement();
			GraphComponent value = (GraphComponent) this.distortions.get(key);
			if(!key.getId().equals(Constants.EPS_ID) && 
					!value.getId().equals(Constants.EPS_ID)){
					if(res==Double.MAX_VALUE){
						res=0;
					}
					count++;
					res+=Constants.costFunction.getCosts(key, value);
			}
		

		}
		return res;
	}
	

	@Override
	/**
	 * @return the editPath as a string 
	 * EditPath To String converter
	 */
	public String toString() {		  

		/* Format:
		 *  < HeuristicCosts<>totalCosts<>getGraphId1<>getGraphId2<>getEdgesId1<>getEdgesId2<>getEdgesMode1<>getEdgesMode2<>
		 *  unUsedNodes1<>unUsedNodes2<>unUsedEdges1<>unUsedEdges2<>Distortions<>invertedEdges
		 */
		String str=""+HeuristicCosts+"<>"+totalCosts+"<>";
		str=str+source.getId()+"<>"+target.getId()+"<>";
		str=str+source.getEdgeId()+"<>"+target.getEdgeId()+"<>";
		str = str+ source.getEdgeMode()+"<>"+target.getEdgeMode();

		str=str+"<>";
		// Nodes1 Format:  <node1.toString()/node2.toString()/node3.toString()/.........etc>
		for(int i=0; i<unUsedNodes1.size();i++)
		{
			Node node1 = (Node) unUsedNodes1.get(i);
			str =str + node1.toString();
			if(i!=unUsedNodes1.size()-1)
			{
				str=str+"/";
			}
		}

		str=str+"<>";
		// Nodes2 Format:  <node1.toString()/node2.toString()/node3.toString()/.........etc>
		for(int i=0; i<unUsedNodes2.size();i++)
		{
			Node node2 = (Node) unUsedNodes2.get(i);
			str =str + node2.toString();
			if(i!=unUsedNodes2.size()-1)
			{
				str=str+"/";
			} 
		}

		str=str+"<>";
		// Edges1 Format:  <edge1.toString()/edge2.toString()/edge3.toString()/.........etc>
		for(int i=0; i<unUsedEdges1.size();i++)
		{
			Edge edge1 = (Edge) unUsedEdges1.get(i);
			str=str+edge1.toString();
			if(i!=unUsedEdges1.size()-1)
			{
				str=str+"/";
			}
		}


		str=str+"<>";
		// Edges2 Format:  <edge1.toString()/edge2.toString()/edge3.toString()/.........etc>
		for(int i=0; i<unUsedEdges2.size();i++)
		{
			Edge edge2 = (Edge) unUsedEdges2.get(i);
			str=str+edge2.toString();
			if(i!=unUsedEdges2.size()-1)
			{
				str=str+"/";
			}
		}

		str=str+"<>";
		// Distortions:  <distortion1/distortion2/distortion3/.........etc>

		// System.out.println("--Distortions");
		Enumeration enumeration = this.distortions.keys();

		int counter=0; 
		while (enumeration.hasMoreElements())
		{
			if(counter!=0)
			{
				str=str+"/";
			}	
			counter=1;
			GraphComponent key = (GraphComponent) enumeration.nextElement();
			GraphComponent value = (GraphComponent) this.distortions.get(key);
			//System.out.println("--key ="+key.getComponentId()+", value = "+value.getComponentId());

			// edge  --- edge

			if(key.isNode() == false  && value.isNode()==false){

				str=str +"Edge%";
				if(key.getComponentId().equals("eps_id"))
				{
					str=str+"eps_id"+">>";
				}
				else{
					Edge keyEdge = (Edge)key;
					str=str+keyEdge.toString()+">>";
					//System.out.println("-- ketEdge :"+keyEdge.toString());
				}
				if(value.getComponentId().equals("eps_id"))
				{
					str=str+"eps_id";

				}
				else{
					Edge valueEdge =(Edge)value;
					str=str+valueEdge.toString();

				}


			}
			// node 
			else{
				str= str+"Node%";
				if(key.getComponentId().equals("eps_id"))
				{
					str=str+"eps_id"+">>";
				}
				else{
					Node keyNode = (Node)key;
					str=str+keyNode.toString()+">>";
				}
				if(value.getComponentId().equals("eps_id"))
				{
					str=str+"eps_id";
				}
				else{
					Node valueNode =(Node)value;
					str=str+valueNode.toString();
				}
			}
		}

		str=str+"<>";
		// invertedEdges Format: <invertedEdge1/invertedEdge2/ ...... >		
		Enumeration enumeration2 = this.EdgeInverted.keys();
		int counter1=0; 
		while (enumeration.hasMoreElements())
		{
			if(counter1!=0)
			{
				str=str+"/";
			}
			counter=1;
			Edge key = (Edge) enumeration.nextElement();
			String value = this.EdgeInverted.get(key).toString();
			str=str+key.toString()+"="+value;
		}
		return str;
	}


	
	public double ComputeAverageCostsWithoutDeletionAndInsertion() {
		// TODO Auto-generated method stub
		
		double res =0;
		double count=0;
		Enumeration enumeration = this.distortions.keys();
		while (enumeration.hasMoreElements()) {
			GraphComponent key = (GraphComponent) enumeration.nextElement();
			GraphComponent value = (GraphComponent) this.distortions.get(key);
			if(!key.getId().equals(Constants.EPS_ID) && 
					!value.getId().equals(Constants.EPS_ID)){
					
					count++;
					res+=Constants.costFunction.getCosts(key, value);
			}
		

		}
		if(count >0) return res/count;
		if(count ==0) return Double.MAX_VALUE;
		return res;
	}


}
