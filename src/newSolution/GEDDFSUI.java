package newSolution;

import java.awt.EventQueue;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Timer;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileFilter;

import algorithms.Constants;
import algorithms.MatrixGenerator;
import algorithms.Munkres;
import algorithms.MunkresRec;
import util.AscendingOrderMyNodeComparator;
import util.EditPath;
import util.GRECCostFunction1;
import util.Graph;
import util.MyTree;
import util.Node;
import util.UniversalEdgeHandler;
import util.MyTree.MyNode;
import xml.XMLParser;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Font;

/**
 * GEDDFSUI est la classe principale dans laquelle on r�alise l'import des fichiers de graphes
 * 
 * @author valliere
 * @version 0.1
 */
public class GEDDFSUI extends JFrame{

	private JPanel contentPane;
	private int heuristicmethod = EditPath.MunkresAssigmentHeuristic;
	private boolean variableordering = false;
	private AscendingOrderMyNodeComparator MyNodeComparator; 
	private static MyTree OPEN, OPEN2;
	
	private int ubmethod = 0;
	public static int MunkresUB=0; // The first upper bound = munkres
	public static int DummyUB=1; // The first branch in the search tree
	private Node CurVertex, CurVertex1; //Current node to be explored
	private int G2NbNodes; //Number of nodes of graph G2
	boolean debug; // a variable used for debug issues
	
	/**
	 * Lit le contenu d'un fichier
	 * 
	 * @param file
	 * 		Le chemin vers le fichier � lire
	 * 
	 * @return le contenu du fichier sous forme d'une chaine de caract�res.
	 */
	public String readFile(String file) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String ligne;
			StringBuffer fichier = new StringBuffer();
 
			while((ligne = reader.readLine()) != null){
				fichier.append(ligne);
				fichier.append("\n");			
			}
			reader.close();
 
			return fichier.toString();		
		} catch (IOException e) {
			return e.getMessage();
		}
	}
	
	/**
	 * This function calculates the first upper bound which is the first branch in the search tree 
	 * @param p
	 * @return a matching solution type EditPath
	 */
	private EditPath dummyUpperBound(EditPath p) {
		EditPath ptmp=null;
		if(p.isComplete() == false){
			ptmp = new EditPath(p);
			// TODO Auto-generated method stub
			int G1NbNodes = ptmp.getUnUsedNodes1().size();
			int G2NbNodes = ptmp.getUnUsedNodes2().size();
			// Substitution case
			//System.out.println("Substitution phase");
			for(int i=0;i<G1NbNodes  ;i++)
			{

				Node u = (Node) ptmp.getUnUsedNodes1().getFirst();
				if(i<G2NbNodes) {
					Node v = (Node) ptmp.getUnUsedNodes2().getFirst();
					ptmp.addDistortion(u, v);
				}

			}

			//Deletion case
			if(G1NbNodes > G2NbNodes)
			{
				//System.out.println("Deletion phase");
				int noOfDeletedNodes = G1NbNodes- G2NbNodes; 
				for(int i=0;i<noOfDeletedNodes;i++)
				{

					Node u = ptmp.getNext();
					ptmp.addDistortion( u , Constants.EPS_COMPONENT);

				}
			}

			//Insertion case
			else if(G1NbNodes < G2NbNodes)
			{
				//System.out.println("Insertion phase");
				int noOfInsertedNodes = G2NbNodes- G1NbNodes; 
				for(int i=0;i<noOfInsertedNodes;i++)
				{
					Node u = ptmp.getNextG2();
					ptmp.addDistortion( Constants.EPS_COMPONENT , u );	
				}
			}
		}else{
			return p;
		}

		return ptmp;
	}
	
	/**
	 * This function calculates the first upper bound before starting to explore the search tree in a depth first way
	 * @param upperboundmethod
	 * @return a matching solution type EditPath
	 */
	private EditPath ComputeUpperBound(int upperboundmethod) {
		// TODO Auto-generated method stub

		EditPath res=null;
		
		
		// if users don't want to set a first upper bound then the first upper bound = infinity
		if(upperboundmethod ==-1){
			Constants.UBCOST = Double.MAX_VALUE;
			res = null;
		}
		
		// if users want the first upper bound to be munkres ....
		
		if(upperboundmethod == MunkresUB){
			MatrixGenerator mgen = new MatrixGenerator();
			Munkres munkres = new Munkres();
			MunkresRec munkresRec = new MunkresRec();
			mgen.setMunkres(munkresRec);

			double[][] matrix = mgen.getMatrix(Constants.g1, Constants.g2);  //on gen�re la matrice de co�t(noeud+arcs) entre G1 et G2
			munkres.setGraphs(Constants.g1, Constants.g2);
			Constants.UBCOST = munkres.getCosts(matrix);
			res = munkres.ApproximatedEditPath();  //construction d'une solution de matching
			//EditPath ROOT = new EditPath(this.G1,this.G2,this.heuristicmethod); 
			//res = this.dummyUpperBound(ROOT);
			Constants.UBCOST = res.getTotalCosts();

		}

		// if users want the upper bound to be the first branch in the search tree (s'arr�ter apr�s l'exploration de la premi�re branche)

		if(upperboundmethod == this.DummyUB){  
			EditPath ROOT = new EditPath(Constants.g1,Constants.g2); 
			ROOT.setHeuristicmethod(this.heuristicmethod);
			res = this.dummyUpperBound(ROOT);
			Constants.UBCOST = res.getTotalCosts();
		}
		return res;
	}
	
	/**
	  * This function starts the exploration of the search tree in a depth-first way and take the component with cost min
	  * @return a matching solution type EditPath
	 */

	private EditPath loop() {
		
		boolean condition1;
		boolean condition2;
		boolean endnodes = false;
		
		// Testing whether or not OPEN is empty 
		if(OPEN.isEmpty() == true){
			Constants.UB_min = null;
			Constants.UB_minCOST = 0.0;
			return Constants.UB_min; // if OPEN is empty then return the best solution found so far (i.e. the optimal solution)
		}

		EditPath pmin=null;
		MyNode<EditPath> pminNode=null;
		MyNode<EditPath> CurNode=OPEN.root;
	
/*		if(( pminNode == null) && (OPEN.root == null) ){
			//all the tree has been explored since pminNode is null and its CurNode is root.
			//no more children to explore
			return UB;
		}*/
		
		while(endnodes == false){

			// Searching for the node whose distance/cost is the minimum among all the children of CurNode
			pminNode = OPEN.pollFirstLowestCost(CurNode);
			
			//condition1 : Test if there is any child to explore from CurNode
			//condition2 : Test if CurNode is ROOT node
			condition1 = (pminNode == null);
			condition2 = ((pminNode == null)&&(CurNode.parent != null));
			while( condition1 && condition2){
				//BackTrack : pminNode is null so CurNode has no children. so we get the parent of CurNode
				CurNode =OPEN.BackTrack(CurNode);
				//pollFirstLowestCost : get the cheapest Child Node. The node is removed from the tree
				pminNode=OPEN.pollFirstLowestCost(CurNode);	
				condition1 = (pminNode == null);
				condition2 = ((pminNode == null)&&(CurNode.parent != null));
			}

			// pmin and pmin_second are the node which will be expanded 
			pmin = pminNode.data;
			// generates all successors of node u in the search tree
			// and add them to open
								
			if(pmin.getUnUsedNodes1().size() > 0){
				this.CurVertex=pmin.getNext();
				if(debug==true) System.out.println("Current Node="+this.CurVertex.getId());

				// For all the unmatched nodes w in V2 (not yet included in pmin), we add the substituions of the next node Uk+1 in V1 with all w's
				LinkedList<Node>UnUsedNodes2= pmin.getUnUsedNodes2();

				for(int i=0;i<UnUsedNodes2.size();i++){
					EditPath newpath = new EditPath(pmin);
					Node w = UnUsedNodes2.get(i);
					newpath.addDistortion(this.CurVertex, w);
					AddTreeNode(pminNode,newpath,Constants.UBCOST);
				}
		
				// We add the deletion of Uk+1
				EditPath newpath = new EditPath(pmin);
				newpath.addDistortion(this.CurVertex, Constants.EPS_COMPONENT);
				AddTreeNode(pminNode,newpath,Constants.UBCOST);
					
				//next node to be explored
				//pminNode that has been expanded. Children has been created so we need to go deeper
				CurNode=pminNode;
				if(CurNode.children.size() == 0)
				{
					EditPath pathSolution = new EditPath(CurNode.data);
					pathSolution.complete();	
					double g = pathSolution.getTotalCosts();
					double h = pathSolution.ComputeHeuristicCosts(heuristicmethod);
					double f= g+h;
						
					Constants.UB_minCOST = pathSolution.getTotalCosts();
					Constants.UB_min = pathSolution;
					endnodes = true;
					return Constants.UB_min;
				}
			}
			else{
				// if k= the size of the nodes of G1
				// we put in pmin all the insertions of the nodes V2 that are not matched yet in pmin
				// we add pmin in OPEN
						
				EditPath newpath = new EditPath(pmin);
				newpath.complete();	
				double g = newpath.getTotalCosts();
				double h = newpath.ComputeHeuristicCosts(heuristicmethod);
				double f= g+h;
					
				Constants.UB_minCOST = newpath.getTotalCosts();
				Constants.UB_min = newpath;
				endnodes = true;
				return Constants.UB_min;
			}//end of if(pmin.getUnUsedNodes1().size() > 0)
		}//end of while true
		return Constants.UB_min;	
	}
	
	/**
	 * This function starts the exploration of the search tree in a depth-first way and take the component with second cost min
	 * @return a matching solution type EditPath
	 */
	private EditPath loop_minSecond() {
		boolean condition1;
		boolean condition2;
		boolean endnodes = false;
		
		// Testing whether or not OPEN is empty 
		if(OPEN2.isEmpty() == true){
			Constants.UB_minsecond = null;
			Constants.UB_minsecondCOST = 0.0;
			return Constants.UB_minsecond; // if OPEN is empty then return the best solution found so far (i.e. the optimal solution)
		}

		EditPath pmin=null;
		MyNode<EditPath> pminNode=null;
		MyNode<EditPath> CurNode=OPEN2.root;
		
		
		while(endnodes == false){
			
			if((CurNode.children.size() > 1) ){
				
				//pollFirstLowestCost : get the cheapest Child Node. The node is removed from the tree
				pminNode=OPEN2.pollSecondLowestCost(CurNode);
			}
			else{
				// Searching for the node whose distance/cost is the minimum among all the children of CurNode
				pminNode = OPEN2.pollFirstLowestCost(CurNode);
			}
	
			
			//condition1 : Test if there is any child to explore from CurNode
			//condition2 : Test if CurNode is ROOT node
			condition1 = (pminNode == null);
			condition2 = ((pminNode == null)&&(CurNode.parent != null));
			while( condition1 && condition2){
				//BackTrack : pminNode is null so CurNode has no children. so we get the parent of CurNode
				CurNode =OPEN2.BackTrack(CurNode);
				//pollFirstLowestCost : get the cheapest Child Node. The node is removed from the tree
				pminNode=OPEN2.pollFirstLowestCost(CurNode);	
				condition1 = (pminNode == null);
				condition2 = ((pminNode == null)&&(CurNode.parent != null));
			}

			// pmin and pmin_second are the node which will be expanded 
			pmin = pminNode.data;
			// generates all successors of node u in the search tree
			// and add them to open
								
			if(pmin.getUnUsedNodes1().size() > 0){
				this.CurVertex1=pmin.getNext();
				if(debug==true) System.out.println("Current Node="+this.CurVertex1.getId());

				// For all the unmatched nodes w in V2 (not yet included in pmin), we add the substituions of the next node Uk+1 in V1 with all w's
				LinkedList<Node>UnUsedNodes2= pmin.getUnUsedNodes2();

				for(int i=0;i<UnUsedNodes2.size();i++){
					EditPath newpath = new EditPath(pmin);
					Node w = UnUsedNodes2.get(i);
					newpath.addDistortion(this.CurVertex, w);
					AddTreeNode2(pminNode,newpath,Constants.UBCOST);
				}
		
				// We add the deletion of Uk+1
				EditPath newpath = new EditPath(pmin);
				newpath.addDistortion(this.CurVertex, Constants.EPS_COMPONENT);
				AddTreeNode2(pminNode,newpath,Constants.UBCOST);
					
				//next node to be explored
				//pminNode that has been expanded. Children has been created so we need to go deeper
				CurNode=pminNode;
				
				if(CurNode.children.size() == 0)
				{
					EditPath pathSolution = new EditPath(CurNode.data);
					pathSolution.complete();	
					double g = pathSolution.getTotalCosts();
					double h = pathSolution.ComputeHeuristicCosts(heuristicmethod);
					double f= g+h;
						
					Constants.UB_minsecondCOST = pathSolution.getTotalCosts();
					Constants.UB_minsecond = pathSolution;
					endnodes = true;
					return Constants.UB_minsecond;
				}
			}
			else{
				// if k= the size of the nodes of G1
				// we put in pmin all the insertions of the nodes V2 that are not matched yet in pmin
				// we add pmin in OPEN
						
				EditPath newpath = new EditPath(pmin);
				newpath.complete();	
				double g = newpath.getTotalCosts();
				double h = newpath.ComputeHeuristicCosts(heuristicmethod);
				double f= g+h;
					
				Constants.UB_minsecondCOST = newpath.getTotalCosts();
				Constants.UB_minsecond = newpath;
				endnodes = true;
				return Constants.UB_minsecond;
			}//end of if(pmin.getUnUsedNodes1().size() > 0)
		}//end of while true
		return Constants.UB_minsecond;	
	}  
	
	/**
	 * The initialization function of the search tree
	 */
	private void inti() {
		// TODO Auto-generated method stub
		
		for(int i=0;i<G2NbNodes;i++){
			// In OPEN, we put all the insertions of U1 with all the nodes of G2

			EditPath p = new EditPath((EditPath) OPEN.root.data); // An initialization for both G1 and G2 "to show that we did not use neither edges nor nodes of both G1 and G2"
			EditPath p2 = new EditPath((EditPath) OPEN2.root.data);
			Node v = (Node)Constants.g2.get(i); // the current node of G2
			this.CurVertex = p.getNext(); // the curent node of G1
			this.CurVertex1 = p2.getNext(); // the curent node of G1
			/* Add distortion between the current node u1 and each node in G2 
				 distortion means insertion or substitution but not deletion as we are sure that
				 G2 has nodes as we are inside the for loop of G2 */
			p.addDistortion(this.CurVertex, v);
			p2.addDistortion(this.CurVertex1, v);
			AddTreeNode(OPEN.root,p,Constants.UBCOST);
			AddTreeNode2(OPEN2.root,p2,Constants.UBCOST);
			
		}

		// We put in OPEN the deletion of U1.
		EditPath p = new EditPath((EditPath) OPEN.root.data);
		EditPath p2 = new EditPath((EditPath) OPEN2.root.data);
		this.CurVertex = p.getNext();
		this.CurVertex1 = p2.getNext();
		p.addDistortion(this.CurVertex, Constants.EPS_COMPONENT); // Zeina: deletion
		p2.addDistortion(this.CurVertex1, Constants.EPS_COMPONENT);

		if(debug==true) System.out.println("Deletion CurNode="+this.CurVertex.getId() + " -- Cost = "+p.getTotalCosts());
		AddTreeNode(OPEN.root,p,Constants.UBCOST);
		AddTreeNode2(OPEN2.root,p2,Constants.UBCOST);
	}

	/**
	 * add a node to the tree if it is worth. if it is lower than UBCOST. the new node is linked to its parent node.
	 * @param parent
	 * @param p
	 * @param ubcost2
	 */
	private void AddTreeNode(MyNode<EditPath> parent, EditPath p, double ubcost2) {
		// TODO Auto-generated method stub
		double g=p.getTotalCosts();
		double h=p.ComputeHeuristicCosts(heuristicmethod);
		double f = g+h;
		if(f<ubcost2)
		{
			OPEN.Add(parent,p);	
		}
	}

	private void AddTreeNode2(MyNode<EditPath> parent, EditPath p, double ubcost2) {
		// TODO Auto-generated method stub
		double g=p.getTotalCosts();
		double h=p.ComputeHeuristicCosts(heuristicmethod);
		double f = g+h;
		if(f<ubcost2)
		{
			OPEN2.Add(parent,p);	
		}
	}
	
	/**
	 * Create the frame.
	 */
	public GEDDFSUI() {

		final FileFilter filter = new FileFilter() {
			 
			@Override
			public String getDescription() {
				return "Fichiers xml";
			}
 
			@Override
			public boolean accept(File arg0) {
				if(arg0.getName().endsWith("gxl"))return true;
				return false;
			}
		};
		setTitle("Application de matching de graphes");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 492, 299);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(253, 245, 230));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblApplicationDeMatching = new JLabel("Application de matching de graphes");
		lblApplicationDeMatching.setFont(new Font("Cambria Math", Font.BOLD | Font.ITALIC, 19));
		lblApplicationDeMatching.setForeground(new Color(128, 0, 0));
		lblApplicationDeMatching.setBounds(101, 11, 348, 27);
		contentPane.add(lblApplicationDeMatching);
		
		final JTextArea textArea = new JTextArea();
		textArea.setBounds(149, 49, 317, 96);
		contentPane.add(textArea);
		
		final JTextArea textArea_1 = new JTextArea();
		textArea_1.setBounds(149, 147, 317, 102);
		contentPane.add(textArea_1);
		
		JButton btnImporterFichiers = new JButton("Importer fichiers");
		btnImporterFichiers.setFont(new Font("Cambria", Font.BOLD, 11));
		btnImporterFichiers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser chooser = new JFileChooser();
				//s�lection multiple de fichiers
				chooser.setMultiSelectionEnabled(true);
				
				chooser.setFileFilter(filter);
				
				//chooser.isAcceptAllFileFilterUsed();
	 
				int returnVal = chooser.showOpenDialog(contentPane);
				String s1 = null, s2=null;
				if(returnVal==JFileChooser.APPROVE_OPTION){
					File[] files = chooser.getSelectedFiles();
					if(files.length == 2){
						s1 = readFile(files[0].getPath());
						s2 = readFile(files[1].getPath());
					}
					textArea.setText(s1);
					textArea_1.setText(s2);
									
					try {
						Constants.timer = new Timer();	
						
						Constants.timeconstraint = 30000;  // time limit: 30 seconds
						Constants.costFunction = new GRECCostFunction1();
						Constants.edgeHandler = new UniversalEdgeHandler();
						
						XMLParser xmlParser = new XMLParser();
						Constants.g1=xmlParser.parseGXL(files[0].getAbsolutePath().toString());
						Constants.g2=xmlParser.parseGXL(files[1].getAbsolutePath().toString());
						
						Constants.ROOT = new EditPath(Constants.g1,Constants.g2,heuristicmethod,variableordering);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});
		btnImporterFichiers.setBounds(0, 50, 139, 23);
		contentPane.add(btnImporterFichiers);
		
		JButton btnCostNodes = new JButton("Racine - noeuds");
		btnCostNodes.setFont(new Font("Cambria", Font.BOLD, 11));
		btnCostNodes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new CostMatrixNodesUI().setVisible(true);
			}
		});
		btnCostNodes.setBounds(0, 104, 139, 23);
		contentPane.add(btnCostNodes);
		
		JButton btnCostEdges = new JButton("Racine - arcs");
		btnCostEdges.setFont(new Font("Cambria", Font.BOLD, 11));
		btnCostEdges.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new CostMatrixEdgesUI().setVisible(true);
			}
		});
		btnCostEdges.setBounds(0, 165, 139, 23);
		contentPane.add(btnCostEdges);  
		
		JButton btnUB = new JButton("Solutions - matching");
		btnUB.setFont(new Font("Cambria", Font.BOLD, 11));
		btnUB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MyNodeComparator =new AscendingOrderMyNodeComparator(heuristicmethod); // Initialization: Integrating the heuristic method for re-ordering nodes
				OPEN = new MyTree<EditPath>(Constants.ROOT,MyNodeComparator); // Initializing the OPEN list
				// Initializing the OPEN list again for calculate another solution
				OPEN2 = new MyTree<EditPath>(Constants.ROOT,MyNodeComparator);
				// Testing whether the first upper bound has to be munkres or the first branch in the search tree
				if(Constants.FirstUB==null || ubmethod!=MunkresUB){
					//avant la boucle
					Constants.UB = ComputeUpperBound(ubmethod);
				}else{
					Constants.UB = Constants.FirstUB;
					Constants.UBCOST=Constants.UB.getTotalCosts();
					Constants.FirstUB=null;
				}
				
				// Generating the first level of the search tree
				inti();
				
				//branch and bound for every solution
				Constants.UB_min = loop();
			 
				Constants.UB_minsecond = loop_minSecond();
				
				/*System.out.println("UB : " +Constants.UB.bestMatchingNodesMapping());
				System.out.println("UBmin : " +Constants.UB_min.bestMatchingNodesMapping());
				System.out.println("UBmin2 : " +Constants.UB_minsecond.bestMatchingNodesMapping());*/
				
				new EditPathUI().setVisible(true);
			}
		});
		btnUB.setBounds(0, 226, 139, 23);
		contentPane.add(btnUB);
	}
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GEDDFSUI frame = new GEDDFSUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
