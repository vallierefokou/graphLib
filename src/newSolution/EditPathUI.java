package newSolution;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JSeparator;
import javax.swing.SwingConstants;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JRadioButton;

import algorithms.Constants;

import javax.swing.JTextArea;

/**
 * EditPathUI est la classe permettant d'afficher les diff�rentes solutions de matching
 * 
 * @author valliere 
 * @version 0.1
 */
public class EditPathUI extends JDialog {

	private final JPanel contentPanel = new JPanel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			EditPathUI dialog = new EditPathUI();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public EditPathUI() {
		setTitle("Choix de solutions de matching");
		setBounds(100, 100, 705, 583);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPanel.setBackground(new Color(253, 245, 230));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
			
			JLabel lblRsultatsDeCalculs = new JLabel("Choix de solution de Matching");
			lblRsultatsDeCalculs.setForeground(new Color(128, 0, 0));
			lblRsultatsDeCalculs.setFont(new Font("Cambria Math", Font.BOLD | Font.ITALIC, 20));
			lblRsultatsDeCalculs.setBackground(new Color(128, 0, 0));
			lblRsultatsDeCalculs.setBounds(206, 11, 310, 41);
			contentPanel.add(lblRsultatsDeCalculs);
			
			JPanel panel = new JPanel();
			panel.setLayout(null);
			panel.setBackground(new Color(253, 245, 230));
			panel.setBounds(37, 87, 622, 413);
			contentPanel.add(panel);
			
			JSeparator separator = new JSeparator();
			separator.setOrientation(SwingConstants.VERTICAL);
			separator.setForeground(new Color(169, 169, 169));
			separator.setBounds(204, 0, 38, 413);
			panel.add(separator);
			
			JSeparator separator_1 = new JSeparator();
			separator_1.setOrientation(SwingConstants.VERTICAL);
			separator_1.setForeground(new Color(169, 169, 169));
			separator_1.setBounds(418, 0, 38, 413);
			panel.add(separator_1);
			
			ButtonGroup btnGroup = new ButtonGroup();
			
			JRadioButton rdbtnNewRadioButton = new JRadioButton("Solution 1");
			rdbtnNewRadioButton.setFont(new Font("Times New Roman", Font.BOLD, 14));
			rdbtnNewRadioButton.setBackground(new Color(253, 245, 230));
			rdbtnNewRadioButton.setBounds(44, 7, 109, 23);
			btnGroup.add(rdbtnNewRadioButton);
			panel.add(rdbtnNewRadioButton);
			
			JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("Solution 2");
			rdbtnNewRadioButton_1.setFont(new Font("Times New Roman", Font.BOLD, 14));
			rdbtnNewRadioButton_1.setBackground(new Color(253, 245, 230));
			rdbtnNewRadioButton_1.setBounds(248, 7, 109, 23);
			btnGroup.add(rdbtnNewRadioButton_1);
			panel.add(rdbtnNewRadioButton_1);
			
			JRadioButton rdbtnNewRadioButton_2 = new JRadioButton("Solution 3");
			rdbtnNewRadioButton_2.setFont(new Font("Times New Roman", Font.BOLD, 14));
			rdbtnNewRadioButton_2.setBackground(new Color(253, 245, 230));
			rdbtnNewRadioButton_2.setBounds(472, 7, 109, 23);
			btnGroup.add(rdbtnNewRadioButton_2);
			panel.add(rdbtnNewRadioButton_2);
			
			JLabel lblNewLabel = new JLabel("Co\u00FBt Total  :");
			lblNewLabel.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
			lblNewLabel.setBounds(10, 52, 86, 14);
			panel.add(lblNewLabel);
			
			JLabel lblNewLabel_1 = new JLabel("Co\u00FBt Total  :");
			lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
			lblNewLabel_1.setBounds(217, 52, 86, 14);
			panel.add(lblNewLabel_1);
			
			JLabel lblNewLabel_2 = new JLabel("Co\u00FBt Total  :");
			lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
			lblNewLabel_2.setBounds(429, 52, 88, 14);
			panel.add(lblNewLabel_2);
			
			JLabel lblNewLabel_3 = new JLabel("New label");
			lblNewLabel_3.setBounds(106, 49, 75, 23);
			panel.add(lblNewLabel_3);
			
			JLabel lblNewLabel_4 = new JLabel("New label");
			lblNewLabel_4.setBounds(313, 53, 75, 14);
			panel.add(lblNewLabel_4);
			
			JLabel lblNewLabel_5 = new JLabel("New label");
			lblNewLabel_5.setBounds(516, 53, 96, 14);
			panel.add(lblNewLabel_5);
			
			JLabel lblSlectionnerLaSolution = new JLabel("S\u00E9lectionnez la solution de votre choix :");
			lblSlectionnerLaSolution.setFont(new Font("Yu Gothic", Font.ITALIC, 14));
			lblSlectionnerLaSolution.setBounds(29, 62, 275, 14);
			contentPanel.add(lblSlectionnerLaSolution);
			
			JTextArea textArea = new JTextArea();
			textArea.setBounds(10, 83, 184, 319);
			textArea.setLineWrap(true);
			panel.add(textArea);
			
			JTextArea textArea_1 = new JTextArea();
			textArea_1.setBounds(215, 83, 193, 319);
			textArea_1.setLineWrap(true);
			panel.add(textArea_1);
			
			JTextArea textArea_2 = new JTextArea();
			textArea_2.setBounds(429, 83, 183, 319);
			textArea_2.setLineWrap(true);
			panel.add(textArea_2);
			
			
			//display elements of differents solutions (cost + solution)
			lblNewLabel_3.setText(String.valueOf(Constants.UBCOST));
			lblNewLabel_4.setText(String.valueOf(Constants.UB_minCOST));
			lblNewLabel_5.setText(String.valueOf(Constants.UB_minsecondCOST));
			
			textArea.setText(Constants.UB.toString());
			textArea_1.setText(Constants.UB_min.toString());
			textArea_2.setText(Constants.UB_minsecond.toString());
			
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			buttonPane.setBackground(new Color(253, 245, 230));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			
			JButton btnNewButton = new JButton("Valider");
			btnNewButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if(rdbtnNewRadioButton.isSelected()){
						Constants.UBSelected = Constants.UB;
						dispose();
						new UBSolutionUI().setVisible(true);
					}
					else if(rdbtnNewRadioButton_1.isSelected()){
						Constants.UBSelected = Constants.UB_min;
						dispose();
						new UBSolutionUI().setVisible(true);
					}
					else if(rdbtnNewRadioButton_2.isSelected()){
						Constants.UBSelected = Constants.UB_minsecond;
						dispose();
						new UBSolutionUI().setVisible(true);
					}
					else{
						//Bo�te du message d'erreur
						JOptionPane jop;
						jop = new JOptionPane();
						jop.showMessageDialog(null, "Veuillez s�lectionner une solution", "Erreur", JOptionPane.ERROR_MESSAGE);
					}
				}
			});
			btnNewButton.setFont(new Font("Cambria", Font.BOLD, 11));
			buttonPane.add(btnNewButton);
		}
	}