package newSolution;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.AbstractListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListModel;
import javax.swing.table.AbstractTableModel;

import algorithms.Constants;

import javax.swing.ScrollPaneConstants;

import util.GraphComponent;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.LinkedList;
import java.awt.Font;

/**
 * CostMatrixNodesUI est la classe permettant de consulter et modifier la matrice de co�ts des noeuds du graphe
 * 
 * @author valliere
 * @version 0.1
 */
public class CostMatrixNodesUI extends JDialog {

	private final JPanel contentPanel = new JPanel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			CostMatrixNodesUI dialog = new CostMatrixNodesUI();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public CostMatrixNodesUI() {
		setBounds(100, 100, 481, 300);
		setTitle("Matrice de co�ts de noeuds");
		setSize(1000,300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new BorderLayout());
		contentPanel.setBackground(new Color(253, 245, 230));
		//contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPanel.setSize(600, 300);
		
		LinkedList ListNodes1 = new LinkedList();
		ListNodes1.addAll(Constants.g1);
		String[] header_title = new String[Constants.nodecostmatrix[0].length];
		

		for(int i = 0; i < ListNodes1.size(); i++){
			GraphComponent currentnode = (GraphComponent)ListNodes1.get(i);
			header_title[i] = currentnode.getTable().get("type").toString();
		}
		header_title[ListNodes1.size()] = "supp";
		
		ListModel lm = new AbstractListModel(){

			@Override
			public Object getElementAt(int index) {
				// TODO Auto-generated method stub
				return header_title[index];
			}

			@Override
			public int getSize() {
				// TODO Auto-generated method stub
				return Constants.nodecostmatrix[0].length;
			}
			
		};
		
		//affichage du tableau de co�t de noeuds
		
		NodesModel model = new NodesModel();
		
		JTable tabCostNode = new JTable(model);
		
		JList rowHeader = new JList(lm);
		rowHeader.setOpaque(false);
	    rowHeader.setFixedCellWidth(80);
		
		tabCostNode.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		
	    JScrollPane scrollPane = new JScrollPane(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	    scrollPane.setRowHeaderView(rowHeader);
	    scrollPane.setViewportView(tabCostNode);
		
	    contentPanel.add(scrollPane);
	    
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			buttonPane.setBackground(new Color(253, 245, 230));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setFont(new Font("Cambria", Font.BOLD, 11));
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						
						//r�cup�rer le tableau modifi�
						for (int i = 0; i < model.getRowCount(); ++i) {
					        for (int j = 0; j < model.getColumnCount(); ++j) {
					        	
					        	Object newValue = model.getValueAt(i, j);
					        	model.setValueAt(newValue, i, j);
					        	Constants.nodecostmatrix[i][j] = model.tabnodecostmatrix[i][j].doubleValue();
					        	System.out.println(Constants.nodecostmatrix[i][j]);
					        }
					        System.out.println("\n");
					    }
						dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Annuler");
				cancelButton.setFont(new Font("Cambria", Font.BOLD, 11));
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						//ne pas conserver les modifications du tableau 
						dispose();
					}
				});
				cancelButton.setActionCommand("Annuler");
				buttonPane.add(cancelButton);
			}
		}
	}
	//Classe mod�le personnalis�e
	  class NodesModel extends AbstractTableModel{
		
	    private final Double[][] tabnodecostmatrix;
	    private final String[] title;
	    
	    //constructeur
	    NodesModel()
	    {
	    	tabnodecostmatrix = new Double[Constants.nodecostmatrix.length][];
	    	title = new String[Constants.nodecostmatrix[0].length];
	    	
	    	LinkedList ListNodes2 = new LinkedList();
			ListNodes2.addAll(Constants.g2);
			

			for(int i = 0; i < ListNodes2.size(); i++){
				GraphComponent currentnode = (GraphComponent)ListNodes2.get(i);
				title[i] = currentnode.getTable().get("type").toString();
			}
			title[ListNodes2.size()] = "supp";
	    	
	    	for( int i = 0; i< Constants.nodecostmatrix.length; ++i){
				tabnodecostmatrix[i]= new Double [Constants.nodecostmatrix[i].length];
			}
			for (int i = 0; i < Constants.nodecostmatrix.length; ++i) {
		        for (int j = 0; j < Constants.nodecostmatrix[i].length; ++j) {
		        	tabnodecostmatrix[i][j] = Constants.nodecostmatrix[i][j];
		        }
		    }
	    }
	  
	    //Retourne le nombre de colonnes
	    public int getColumnCount() {
	      return this.title.length;
	    }

	    //Retourne le nombre de lignes
	    public int getRowCount() {
	      return this.tabnodecostmatrix.length;
	    }

	    //Retourne la valeur � l'emplacement sp�cifi�
	    public Object getValueAt(int row, int col) {
	      return this.tabnodecostmatrix[row][col];
	    }  
	    
	    /**
	    * Retourne le titre de la colonne � l'indice sp�cifi�
	    */
	    public String getColumnName(int col) {
	      return this.title[col];
	    }
	    
	    @Override
	    public boolean isCellEditable(int rowIndex, int columnIndex) {
	        return true; //Toutes les cellules �ditables
	    }
	    
	    @Override
	    public void setValueAt(Object newValue, int rowIndex, int columnIndex)
	    {
	    	this.tabnodecostmatrix[rowIndex][columnIndex] = Double.valueOf(newValue.toString());
	        fireTableCellUpdated(rowIndex,columnIndex); //indiquer � tous les listeners que le contenu de la cellule a chang�
	    }
	  }

}
