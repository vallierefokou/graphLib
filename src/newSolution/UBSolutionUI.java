package newSolution;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Color;

import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JTextArea;

import algorithms.Constants;

/**
 * UBSolutionUI est la classe permettant d'afficher la solution de matching sélectionnée par l'utilisateur
 * 
 * @author valliere 
 * @version 0.1
 */
public class UBSolutionUI extends JDialog {

	private final JPanel contentPanel = new JPanel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UBSolutionUI dialog = new UBSolutionUI();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public UBSolutionUI() {
		setTitle("Meilleure solution de matching");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBackground(new Color(253, 245, 230));
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel label = new JLabel("Meilleure solution de Matching");
		label.setForeground(new Color(128, 0, 0));
		label.setFont(new Font("Cambria Math", Font.BOLD | Font.ITALIC, 20));
		label.setBackground(new Color(128, 0, 0));
		label.setBounds(67, 11, 312, 25);
		contentPanel.add(label);
		
		JTextArea textArea = new JTextArea();
		textArea.setBounds(26, 121, 380, 110);
		textArea.setLineWrap(true);
		contentPanel.add(textArea);
		textArea.setText(Constants.UBSelected.bestMatchingNodesMapping());
		
		JLabel lblCidessousLaMeilleure = new JLabel("Ci-dessous la meilleure solution de matching :");
		lblCidessousLaMeilleure.setFont(new Font("Yu Gothic", Font.ITALIC, 14));
		lblCidessousLaMeilleure.setBounds(27, 72, 328, 14);
		contentPanel.add(lblCidessousLaMeilleure);
	}
}
