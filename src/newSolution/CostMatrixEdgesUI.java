package newSolution;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.AbstractListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.AbstractTableModel;

import util.GraphComponent;
import algorithms.Constants;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.LinkedList;
import java.awt.Font;

/**
 * CostMatrixEdgesUI est la classe permettant de consulter et modifier la matrice de co�ts des arcs du graphe
 * 
 * @author valliere
 * @version 0.1
 */
public class CostMatrixEdgesUI extends JDialog {

	private final JPanel contentPanel = new JPanel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			CostMatrixEdgesUI dialog = new CostMatrixEdgesUI();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public CostMatrixEdgesUI() {
		setBounds(100, 100, 450, 300);
		setTitle("Matrice de co�ts des arcs");
		setSize(1000,300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new BorderLayout());
		contentPanel.setBackground(new Color(253, 245, 230));
		//contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPanel.setSize(600, 300);
		
		LinkedList ListEdges1 = new LinkedList();
		ListEdges1.addAll(Constants.g1.getEdges());
		String[] header_title = new String[Constants.edgecostmatrix[0].length];
		

		for(int i = 0; i < ListEdges1.size(); i++){
			GraphComponent currentedge = (GraphComponent)ListEdges1.get(i);
			header_title[i] = currentedge.getTable().get("type0").toString();
		}
		header_title[ListEdges1.size()] = "supp";
		
		ListModel lm = new AbstractListModel(){

			@Override
			public Object getElementAt(int index) {
				// TODO Auto-generated method stub
				return header_title[index];
			}

			@Override
			public int getSize() {
				// TODO Auto-generated method stub
				return Constants.edgecostmatrix[0].length;
			}
			
		};
		
		//affichage du tableau de co�t d'arcs
		EdgesModel model = new EdgesModel();
		

		JTable tabCostEdge = new JTable(model);
		
		JList rowHeader = new JList(lm);
		rowHeader.setOpaque(false);
	    rowHeader.setFixedCellWidth(50);
	    
		tabCostEdge.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		
		//JScrollPane scrollPane = new JScrollPane(tabCostEdge, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		JScrollPane scrollPane = new JScrollPane(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setViewportView(tabCostEdge);
		scrollPane.setRowHeaderView(rowHeader);
		contentPanel.add(scrollPane);
	    
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			buttonPane.setBackground(new Color(253, 245, 230));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setFont(new Font("Cambria", Font.BOLD, 11));
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						//r�cup�rer le tableau modifi�
						for (int i = 0; i < model.getRowCount(); ++i) {
					        for (int j = 0; j < model.getColumnCount(); ++j) {
					        	
					        	Object newValue = model.getValueAt(i, j);
					        	model.setValueAt(newValue, i, j);
					        	Constants.edgecostmatrix[i][j] = model.tabedgecostmatrix[i][j].doubleValue();
					        	System.out.println(Constants.edgecostmatrix[i][j]);
					        }
					        System.out.println("\n");
					    }
						dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Annuler");
				cancelButton.setFont(new Font("Cambria", Font.BOLD, 11));
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Annuler");
				buttonPane.add(cancelButton);
			}
		}
	}
	
	//Classe mod�le personnalis�e
	  class EdgesModel extends AbstractTableModel{
		
	    private final Double[][] tabedgecostmatrix;
	    private final String[] title;
	    
	    //constructeur
	    EdgesModel()
	    {
	    	tabedgecostmatrix = new Double[Constants.edgecostmatrix.length][];
			title = new String[Constants.edgecostmatrix[0].length];
					
			LinkedList ListEdges2 = new LinkedList();
			
			ListEdges2.addAll(Constants.g2.getEdges());
			
			for(int j = 0; j < ListEdges2.size(); j++){
				GraphComponent currentedge = (GraphComponent)ListEdges2.get(j);
				title[j] = currentedge.getTable().get("type0").toString();
			}
			title[ListEdges2.size()] = "supp";
			
			for( int i = 0; i< Constants.edgecostmatrix.length; ++i){
				tabedgecostmatrix[i]= new Double [Constants.edgecostmatrix[i].length];
			}
			for (int i = 0; i < Constants.edgecostmatrix.length; ++i) {
				for (int j = 0; j < Constants.edgecostmatrix[i].length; ++j) {
					tabedgecostmatrix[i][j] = Constants.edgecostmatrix[i][j];
				}
			}
	    }
	  
	    //Retourne le nombre de colonnes
	    public int getColumnCount() {
	      return this.title.length;
	    }

	    //Retourne le nombre de lignes
	    public int getRowCount() {
	      return this.tabedgecostmatrix.length;
	    }

	    //Retourne la valeur � l'emplacement sp�cifi�
	    public Object getValueAt(int row, int col) {
	      return this.tabedgecostmatrix[row][col];
	    }  
	    
	    /**
	    * Retourne le titre de la colonne � l'indice sp�cifi�
	    */
	    public String getColumnName(int col) {
	      return this.title[col];
	    }
	    
	    @Override
	    public boolean isCellEditable(int rowIndex, int columnIndex) {
	        return true; //Toutes les cellules �ditables
	    }
	    
	    @Override
	    public void setValueAt(Object newValue, int rowIndex, int columnIndex)
	    {
	    	this.tabedgecostmatrix[rowIndex][columnIndex] = Double.valueOf(newValue.toString());
	        fireTableCellUpdated(rowIndex,columnIndex); //indiquer � tous les listeners que le contenu de la cellule a chang�
	    }
	  }
}
